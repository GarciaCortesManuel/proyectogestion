<?php 
//$User=$_POST['User'];
//$Mail=$_POST['mail'];
$dbconn3 = pg_connect("host=localhost port=5444 dbname=Panaderia user=enterprisedb password=101297");
//$log="INSERT INTO panaderia.trabajador (nombre,correo_elec) VALUES ('$User', '$Mail') ";
//$log=pg_query($log);
$inserEmp="";
$editarp="";
$borrarp="";
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Panaderia Adan/Empleado</title>
    <link rel="shortcut icon" href="imagenes/icono.png" type="image/x-icon">
    <link rel="stylesheet" href="estilo.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans&display=swap" rel="stylesheet">

     <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet">

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Dancing+Script:wght@700&display=swap" rel="stylesheet">  

      <script src="https://code.jquery.com/jquery-3.2.1.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    setTimeout(function() {
        $(".ok").fadeOut(1500);
    },3000);
});
</script>

</head>
<body>
    <header>
        <nav>
            <a href="index.html">Inicio</a>
            <a href="#">Productos </a>
            <a href="#">Servicios</a>
            <a href="registroc.html">Usuario</a>
            <a href="registroe.html">Empleado</a>
            <a href="#">Acerca de</a>
            <a href="#">Carrito de Compras</a>
        </nav>
         <section class="textos-header">
            <h1>Panaderia Adan</h1>
            <h2>Panes con sabor y color</h2>
        </section>
        <div class="wave" style="height: 150px; overflow: hidden;" ><svg viewBox="0 0 500 150" preserveAspectRatio="none"
            style="height: 100%; width: 100%;">
            <path d="M-8.66,81.13 C202.97,214.54 305.68,-14.45 500.00,50.09 L500.00,150.33 L0.00,150.33 Z"
                style="stroke: none; fill: rgb(250, 250, 250);"></path>
            </svg></div>
    </header>
    <main>
        
        <section class="Loguin">
            <h2>
                Bienvenido
            </h2>
            <div class="botones">
            <div class="bot1"><a href="#miModal"> Agregar Producto</a></div>
            <div class="bot2"><a href="#miModal2">Actualizar producto</a></div> 
            <div class="bot3"> <a href="#miModal3">Eliminar producto</a></div>
           <div class="bot4"><a>generar reporte</a></div>
          </div>
        </section>
   <div id="miModal" class="modal">
    <div class="modal-contenido">
    <a href="#">X</a>
    <h2>Registro de Productos</h2>
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
       <p><label>Codigo Pan:<input type="number" name="codigo" value="" required></label></p>
       <p><label>Descripcion:<input type="text" name="descri" value="" required></label></p>
       <p><label>Precio venta:<input type="number" name="precio" step="any" value="" required></label></p>
       <p><label>Existencia:<input type="number" name="existencia" value="" required></label></p>
       <p><a href="#"><input type="submit" name="Agregar" value="Agregar"></a></p>
       <?php
           if (isset($_POST["Agregar"]) && $_SERVER["REQUEST_METHOD"] == "POST") {
 	
   $inserEmp=sprintf("INSERT INTO panaderia.producto
VALUES('%s','%s','%s','%s')",$_POST['codigo'],$_POST['descri'],$_POST['precio'],$_POST['existencia']);
   $inserEmp=pg_query($inserEmp);
  
 }
       ?>
    </form>
  </div>  
</div>
  <div id="miModal2" class="modal2">
    <div class="modal-contenido2">
    <a href="#">X</a>
    <h2>Editar producto</h2>
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
       <p><label>Codigo Pan:<select name="codigo">
         <?php  
         $codes=pg_query($dbconn3,"SELECT codigo_pan From panaderia.producto");
         while ($row=pg_fetch_row($codes)) {
             echo '<option value="'.$row[0].'">'.$row[0].'</option>';
         }
         ?>
       </select></label></p>
       <p><label>Descripcion:<input type="text" name="descri" value="" required></label></p>
       <p><label>Precio:<input type="number" step="any" name="precio" value="" required></label></p>
       <p><label>Existencia:<input type="number" name="existencia" value="" required></label></p>
       <p><a href="#ok"><input type="submit" name="Editar" value="Editar"></a></p>
       <?php 
      
        if (isset($_POST["Editar"]) && $_SERVER["REQUEST_METHOD"] == "POST"){
          $editarp="UPDATE panaderia.producto SET precio_v=$_POST[precio],descripcion='$_POST[descri]', existencia=$_POST[existencia] WHERE codigo_pan='" . $_POST["codigo"] ."'";
          $editarp=pg_query($editarp);
        
        }
       ?>
    </form>
  </div>
</div>
<div id="miModal3" class="modal3">
    <div class="modal-contenido3">
    <a href="#">X</a>
    <h2>Eliminar producto</h2>
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
       <p><label>Codigo Pan:<select name="codigo">
         <?php  
         $codes=pg_query($dbconn3,"SELECT codigo_pan From panaderia.producto");
         while ($row=pg_fetch_row($codes)) {
             echo '<option value="'.$row[0].'">'.$row[0].'</option>';
         }
         ?>
       </select></label></p>
       
       <p><a href="#"><input type="submit" name="Eliminar" value="Eliminar"></a></p>
        <?php 
         if (isset($_POST["Eliminar"]) && $_SERVER["REQUEST_METHOD"] == "POST"){
          $borrarp = "DELETE FROM panaderia.producto WHERE codigo_pan='" . $_POST["codigo"] . "'";
          $borrarp = pg_query($borrarp);
         } 
       ?>
    </form>
  </div>
</div>
<div align="center" class="ok" id="ok">
    <?php 
    /*if ($log) {
     echo "Registro Exitoso";
    } else {
      header('Location: registroe.html');
    }*/
    
    if ($borrarp) {
      echo "Producto eliminado";
    }
    
    if ($inserEmp) {
      echo "Producto Agregado";
    }

    if ($editarp) {
      echo "Producto actualizado";
    }
    ?>
    </div>
</main>
</body>
</html>
